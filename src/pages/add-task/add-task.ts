import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskProvider } from '../../providers/task/task';

/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {
	
	public task: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public provider: TaskProvider) {
  }
  
  addTask(){
	  //Add task to array
	this.provider.add(this.task);

	
	// go back to previous screen
	if( this.navCtrl.canGoBack() ){
		this.navCtrl.pop();
	}
  }

}
